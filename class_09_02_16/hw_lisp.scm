(define (square x) (* x x))
(define pi 3.1459)
(define (volume-cylinder r h) (* pi h (square r)))

(define (area-cylinder r h) (+ (* 2 pi (square r)) (* 2 pi r h)))

(define (pipe-area in_r length thick) 
	(/ (area-cylinder thick length) (volume-cylinder thick length)))

(define (speed g time) (* g time))
(define (height g time) (* (/ 1 2) (speed g time) time))