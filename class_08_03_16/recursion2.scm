(define (sum-pi a b)
	(if (> a b)
		0
		(+ (/ (expt -1.0 a) (+ (* 2 a) 1) (sum-pi (+ a 1) b)))))


(define (cube x) (* x x x))

(define (leib_term x) (/ (expt -1 x) (+ 1 (* 2 x))))

(define (inc n) (+ n 1))


(define (identity x) x)

(define (sum term a next b)
	(if (> a b)
		0
		(+ (term a)
			(sum term (next a) next b))))

(define (integral f a b dx)
	(define (add-dx x) (+ x dx))
	(* (sum f (+ a (/ dx 2.0)) add-dx b) dx))

;extra points for next function
(exact->inexact(sum (lambda (i) (/ 1 (* i (+ i 2)))) 1 (lambda (i) (+ i 4)) 1000))

(define (pow_aux b c n)
	(if (>= c n) 1 (* b (pow_aux b (+ c 1) n)))
)

(define (pow b n)
	(if (= n 0) 1 (* b (pow b (- n 1))))
)


(define (fac-iter p c n)
	(if (>= c n) p (fac-iter (* p (+ c 1)) (+ c 1) n))
)

(define (fac n)
	(fac-iter 1 1 n)
)
