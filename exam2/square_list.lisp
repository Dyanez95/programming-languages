(define (square x) (* x x))

(define (square_list x) (invert_list (aux_sqr (cdr x) (car x) '()) '()))

(define (aux_sqr x n l)
	(if
		(null? x)
		(cons (square n) l)
		(aux_sqr (cdr x) (car x) (cons (square n) l))
	)
)

(define (invert_list x a)
	(if
		(null? x)
		a
		(invert_list (cdr x) (cons (car x) a))
	)
)


