#include <stdio.h>

extern float resistor_fun(float);

int main(){
	float Resistor,current;
	
	printf("Enter the current value\n");
	scanf("%f",&current);

	Resistor=resistor_fun(current);

	printf("The required resistor should be \n");
	printf("%1.0f Ohms\n", Resistor);
	return 0;
}