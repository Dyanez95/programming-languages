/*
	Exec4 19/01/16
	Diego Yanez
*/
#define BIGNUM 1000000
#include <stdio.h>

int main(void){
	int a=BIGNUM;

	printf("Hello, world, I am %d happy units\n", a);

	return 0;
}