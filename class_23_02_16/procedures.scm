(define (abs1 x)
	(cond ((> x 0) x)
		((= x 0) 0)
		((< x 0) (- x))))

(define (abs2 x)
	(cond ((< x 0) (- x))
		(else x)))

(define (abs3 x)
	(if (< x 0)
		(- x)
		x))

(define (ex1 n)
	(cond ((<= n 1000) 0.040)
		((<= n 5000) 0.045)
		((<= n 10000) 0.055)
		((> n 10000) 0.060))
)

(define (ex2 n)
	(cond ((<= n 1000) (* 0.040 1000)) ((<= n 5000) (+ (* 1000 0.040) (* (- n 1000) 0.045)))
		(else (+ (* 1000 0.040) (* 4000 0.045) (* (- n 10000) 0.055)))))

(define (ex3 x)
	(and (> x 5) (< x 10)))

(define (>= x y)
	(not (< x y)))

(define (factorial n)
	(if (= n 1) 1 (* n (factorial (- n 1)))))