;obtener cosas de la lista
;(cons 1 '(1 2 (2 3)))
(define lst '(1 2 3))
(cdr (cdr lst))
(car (cdr lst))
(caaddr '(a (b c) (d e f)))
(caar (cdr (cdr '(a (b c) (d e f)))))

;predicados
(define twelve 12)
(number? twelve)
(symbol? 'lol)
(list? (cdr '(31 23)))
(boolean? #f)
(null? '()) ;regresa verdad si se le pasa la lista vacia, falso en cualquier otro caso
(pair? '(a b c))