(define (fac n)
	(cond ((= n 0) 1)
		((> n 0) (* n (fac (- n 1))))
	)
)

(define (fac n)
	(if (= n 1) 1 (* n (fac (- n 1)))))

(define (fib n)
	(cond ((= n 0) 0)
		  ((= n 1) 1)
		  ((> n 1) (+ (fib (- n 1)) (fib (- n 2)))))
	)
)

(define (fact-iter prod counter max-count)
	(if (> counter max-count)
		prod 
		(fact-iter (* counter prod) (+ counter 1) max-count))
	)

(define (fib-iter a b count)
	(if (= count 0)
		b
		(fib-iter (+ a b) a (- count 1))
		)
	)

(define (fibonacci n)
	(fib-iter 1 0 n)
	)

(define (power a b)
	(if (= b 0) 1 (* a (power a (- b 1)))))

;(define (exponential b n)
;	(if (= n 0) 1 (* b (exponential b (- n 1)))))

(define (exp-iter b counter prod)
	(if (= counter 0) prod (exp-iter b (- counter 1) (* prod b))))

(define (exponential b n)
	(exp-iter b n 1))

