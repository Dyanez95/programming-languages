member(X,[X|_]).
member(X,[_|T]):-member(X,T).

count([],0).
count([_|T],R):-count(T,R1),R is R1+1.

suma([],0).
suma([H|T],R):-suma(T,R1), R is R1+H.


is_num_even(X):-mod(X,2)=:=0.

suma_par([],0).
suma_par([H|T],R):-is_num_even(H),suma_par(T,R1),R is R1+H.
suma_par([_|T],R):-suma_par(T,R),!.


#1
print_every_second([]).
print_every_second([_|[X|T]]):-write(X),nl,print_every_second(T).

#2
is_vowel(X):-member(X,[a,e,i,o,u]).

desconsonant([]).
desconsonant([H|T]):- is_vowel(H),write(H),desconsonant(T).
desconsonant([_|T]):- desconsonant(T).

#3
head([],_):-fail.
head([H|_],H).

#4
tail([],_):-fail.
tail([_|T],Y):-tail(T,Y).

#5
last([H],H):-!.
last([_|T],Y):-last(T,Y).

#6
max(X,Y,Y):-X<Y,!.
max(X,Y,X):-Y<X,!.

#7
fact(0,1):-!.
fact(X,R):-Y is X-1,fact(Y,R1),R is R1*X.

#8
fib(0,0):-!.
fib(1,1):-!.
fib(X,R):-	X1 is X-1,
			X2 is X-2,
			fib(X1,R1),
			fib(X2,R2),
			R is R1+R2.

#9
length([],0):-!.
length([H|T],R):-length(T,R1),R is R1+1.

#10
power(X,0,1):-!.
power(X,P,R):-P1 is P-1,power(X,P1,R1),R is X*R1.

#11
count_vowels([],0):-!.
count_vowels([H|T],R):-is_vowel(H),count_vowels(T,R1),R is R1+1.
count_vowels([_|T],R):-count_vowels(T,R).

#12
vowels([],[]):-!.
vowels([H|T],R):-is_vowel(H),vowels(T,R1),R=[H|R1].
vowels([_|T],R):-vowels(T,R).

#13
nple(_,[],[]):-!.
nple(X,[H|T],R):-X1 is H*X,nple(X,T,R1),R=[X1|R1].

#14
del(_,[],[]):-!.
del(X,[X|T],R):-del(X,T,R1),R=R1.
del(X,[H|T],R):-del(X,T,R1),R=[H|R1].

#15
find_every_second_aux([],[],_):-!.
find_every_second_aux([H|T],R,Y):-	is_num_even(Y),
									Y1 is Y+1,
									find_every_second_aux(T,R1,Y1),
									R=[H|R1].

find_every_second_aux([_|T],R,Y):-	Y1 is Y+1,
									find_every_second_aux(T,R,Y1).

find_every_second(X,R):-find_every_second_aux(X,R,1).

#16
sum([],0):-!.
sum([H|T],R):-sum(T,R1),R is R1+H.

#17
nth_aux(_,[],_,_):-fail.
nth_aux(X,[H|_],H,I):-X=:=I.
nth_aux(X,[_|T],R,I):-	I1 is I+1,
						nth_aux(X,T,R,I1).

nth(X,L,R):-nth_aux(X,L,R,1).

#18
dupli([],[]):-!.
dupli([H|T],R):-dupli(T,R1),R=[H|[H|R1]].

#19
split_aux(_,[],[],[],_):-!.
split_aux(I,[H|T],R1,R2,C):-	C=<I,
								C1 is C+1,
								split_aux(I,T,R3,R2,C1),
								R1=[H|R3].

split_aux(I,[H|T],R1,R2,C):-	split_aux(I,T,R1,R4,C),
								R2=[H|R4].

split(I,L,R1,R2):-split_aux(I,L,R1,R2,1).

#20
npli(_,0,[]):-!.
npli([H|T],C,R):-C1 is C-1,npli(T,C1,R1),R=[H|R1].